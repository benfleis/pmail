var _ = require('lodash');
var debug = require('debug')('pmail.server');

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var message_plex = require('./lib/message_plex');

// ----------------------------------------------------------------------------
// Top level http server. Basic shell that binds the bits together.
//

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/views/index.html');
});
app.use('/', express.static('public'));
app.use('/models', express.static('models'));
app.use('/presenters', express.static('presenters'));

// bind websocket and message/status events
io.on('connection', function(socket) {
    debug('ws: connect');
    socket.on('disconnect', function() { debug('ws: disconnect'); });
    socket.on('message', message_plex.enqueue.bind(message_plex));
    message_plex.on('status', socket.emit.bind(socket, 'status'));
});

http.listen(8080, function() {
    console.log('listening on *:8080');
});
