// Status model instantiation

function status(id, ts, status_, error) {
    var rv = {
        id: id || ((Math.random() * 0x7fffffff) | 0).toString(36),
        ts: ts || new Date().getTime(),
        status: status_
    };
    if (error) {
        rv.error = error;
    }
    return rv;
}
        
function status_from_obj(obj) {
    obj = obj || {};
    return status(obj.id, obj.ts, obj.status, obj.error);
}

// ----------------------------------------------------------------------------

// export if in node.js
if (typeof module !== 'undefined' && module.exports) {
    module.exports = {
        status: status,
        status_from_obj: status_from_obj
    };
}
