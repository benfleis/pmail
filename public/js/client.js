// connect websocket, and bind message/status presenters to their events
var socket = io();

var message_pres = new MessagePresenter();
var status_pres = new StatusPresenter();

message_pres.on('message', socket.emit.bind(socket, 'message'));
socket.on('status', status_pres.handle.bind(status_pres));
