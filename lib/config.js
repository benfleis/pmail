// read secrets.json (and other config, if needed), creating and
// exporting a single config object.

var _ = require('lodash');
var debug = require('debug')('pmail.config');
var fs = require('fs');

debug('building configuration');
var config = {};
try {
    var secrets = fs.readFileSync(__dirname + '/../config/secrets.json');
    secrets = JSON.parse(secrets);
    module.exports = _.extend(config, secrets);
}
catch (e) {
    console.log('*** config/secrets.json is unreadable/unparseable. ***');
    process.exit(1);
}

