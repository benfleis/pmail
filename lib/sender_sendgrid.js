var assert = require('assert');
var events = require('events');
var util = require('util');

var _ = require('lodash');
var debug = require('debug')('pmail.sendgrid');
var request = require('request');

var config = require('./config');

// ----------------------------------------------------------------------------
// sendgrid send mechanism via authenticated HTTPS
//

//
// looks like:
//
// curl -X POST  -H "Authorization: Bearer XXXX" -d to=blue@benomatic.org -d 'toname=Blue Man' -d subject=Subjective. -d 'text=This is the content, again.' -d from=orange@benomatic.org -d 'fromname=Orange Man' =>
// {"message":"success"} ||
// {"message": "error", "errors": ["Permission denied, wrong credentials"]}
//

function send(msg, cb) {
    debug('[id=' + msg.id + '] sending');
    request.post(
        {
            url: 'https://api.sendgrid.com/api/mail.send.json',
            auth: {bearer: config.sendgrid.key},
            form: {from: msg.from,
                   to: msg.to,
                   subject: msg.subject,
                   text: msg.content},
            timeout: 5000
        },
        function(err, resp, body) {
            err = decode_errors(msg, err, resp, body);
            err = err ? 'Sendgrid: ' + err : null;
            debug('[id=' + msg.id + '] status: ' + (err || 'ok'));
            cb(err);
        }
    );
}

// This error checking is quick and dirty, and covers the basic cases.
function decode_errors(msg, err, resp, body) {
    if (err) {
        return err;
    }

    var body_obj = resp.headers['content-type'] === 'application/json'
        ? JSON.parse(body)
        : {errors: ['unknown']};

    if (is_success(resp)) {
        assert(body_obj.message = 'success', 'body message != "success".');
        return null;
    }
    if (body_obj.errors) {
        return 'Error: ' + body_obj.errors[0] + '; (http ' + resp.statusCode + ')';
    }
    if (is_request_error(resp)) {
        return 'Request Error: HTTP ' + resp.statusCode;
    }
    if (is_server_error(resp)) {
        return 'Server Error: HTTP ' + resp.statusCode;
    }
    return 'Error: ' + body_obj.errors[0];
}

// useful status code maps
// https://sendgrid.com/docs/API_Reference/Web_API/using_the_web_api.html
// skeptical of including 203-206, but following the docs.
function is_success(resp) {
    return 200 <= resp.statusCode && resp.statusCode < 300;
}

function is_request_error(resp) {
    return 400 <= resp.statusCode && resp.statusCode < 500;
}

function is_server_error(resp) {
    return 500 <= resp.statusCode && resp.statusCode < 600;
}

// ----------------------------------------------------------------------------

module.exports.send = send;
