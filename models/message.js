// Message model instantiation and validation

function message(id, ts, from, to, subject, content) {
    return {
        id: id || ((Math.random() * 0x7fffffff) | 0).toString(36),
        ts: ts || new Date().getTime(),
        from: from.trim(),
        to: to.trim(),
        subject: subject,
        content: content
    };
}

function message_from_obj(obj) {
    obj = obj || {};
    return message(obj.id, obj.ts, obj.from, obj.to, obj.subject, obj.content);
}

var email_re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
function message_get_errors(msg) {
    var errs = [];
    if (!email_re.test(msg.from)) {
        errs.push('invalid field: from');
    }
    if (!email_re.test(msg.to)) {
        errs.push('invalid field: to');
    }
    if (!msg.subject) {
        errs.push('missing subject');
    }
    if (!msg.content) {
        errs.push('missing message');
    }
    return errs.join('; ');
}

// ----------------------------------------------------------------------------

// export if in node.js
if (typeof module !== 'undefined' && module.exports) {
    module.exports = {
        message: message,
        message_from_obj: message_from_obj,
        message_get_errors: message_get_errors
    };
}
