// Status presenter appends messages to the status pane

function StatusPresenter() {
    this.div = $('.status');
    this.div.html('awaiting status...');
}

StatusPresenter.prototype.handle = function(evt) {
    var status = status_from_obj(evt);
    console.log('status', status);
    if (this.div.html() === 'awaiting status...') {
        this.div.html('');
    }
    this.div.children().addClass('outdated');
    this.div.append('<p>' + JSON.stringify(status) + '</p>');
};
