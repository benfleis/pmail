NODEMON = node_modules/nodemon/bin/nodemon.js -w server.js -w lib -w models -w presenters -w views -e js,json -w config

run:
	node server.js

dev:
	DEBUG=pmail.* node $(NODEMON) server.js

dev-verbose:
	DEBUG=* node $(NODEMON) server.js

debug:
	DEBUG=server node debug server.js

setup:
	npm install
