var assert = require('assert');
var events = require('events');
var util = require('util');

var _ = require('lodash');
var debug = require('debug')('pmail.message_plex');

var mailgun = require('./sender_mailgun');
var sendgrid = require('./sender_sendgrid');
var message = require('../models/message');
var status = require('../models/status');

// ----------------------------------------------------------------------------

function MessagePlex() {
    events.EventEmitter.call(this);
    //
    // outstanding == map of
    //   id -> {
    //       message:
    //       senders: [],
    //   }
    // message is original message
    // sends is list of remaining send funcs to try; when exhausted,
    // send final failure message.
    //
    this.outstanding = {};
}
util.inherits(MessagePlex, events.EventEmitter);

MessagePlex.prototype.enqueue = function(msg) {
    assert(this.outstanding[msg.id] === undefined);
    this.outstanding[msg.id] = {
        message: msg,
        senders: _.shuffle([mailgun, sendgrid])
    };
    this._send(msg);
};

//
// Manage sends, trying each available sender until success, or none
// available, then emit a message indicating final failure and cleanup.
//
MessagePlex.prototype._send = function(msg) {
    var self = this,
        sender = self.outstanding[msg.id].senders.pop();
    if (sender === undefined) {
        self.emit('status', status.status(msg.id, null, 'error',
                                          'All senders failed: aborted.'));
        delete self.outstanding[msg.id];
        return;
    }
    sender.send(msg, function(err) {
        self.emit('status', status.status(msg.id, null, err ? 'error' : 'ok', err));
        if (err) {
            self._send(msg);
        }
    });
};

// ----------------------------------------------------------------------------
// just for testing
//

function delay_random_status(msg, cb) {
    setTimeout(function() {
        var resp = _.extend(
            {id: msg.id},
            _.sample([
                {status: 'ok'},
                {status: 'error', error: 'unknown "to:" address'},
                {status: 'error', error: 'no servers available'}
            ])
        );
        debug('status: ' + JSON.stringify(resp));
        cb(resp);
    }, (Math.random() * 2000) | 0);
}

// ----------------------------------------------------------------------------

// singleton message plex is singleton.
module.exports = new MessagePlex();
