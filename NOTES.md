## steps

* shell
    * node server serves index.html
    * client js to connect websocket
    * static index.html page for both views
* structure into MVP
    * define models for Message, Status
* presenter layer:
    * smtp: respond w/ canned/random response events
    * server: bind status events to presenter
    * presenter: push status updates to Views
    * send view: create, bindings from presenter
    * presenter: create/dispatch send event
* implement sender services
* email field validation/failure
* split views up? NO.
* load UI startup/binding code from separate file
* walk all code for cleanup

## hand tests
* invalid to/from email
* basic normal send
* firewall off one of senders
* firewall off both senders
* check emails
