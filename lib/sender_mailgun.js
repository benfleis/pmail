var events = require('events');
var util = require('util');

var _ = require('lodash');
var debug = require('debug')('pmail.mailgun');
var request = require('request');

var config = require('./config');

// ----------------------------------------------------------------------------
// mailgun send mechanism via authenticated HTTPS
//

//
// mailgun example:
//
// curl -s --user 'api:XXXX' \
//     https://api.mailgun.net/v3/sandboxda341bd3d3d742b291bca56af3f5e24c.mailgun.org/messages \
//     -F from='Excited User <mailgun@sandboxda341bd3d3d742b291bca56af3f5e24c.mailgun.org>' \
//     -F to=orange@benomatic.org \
//     -F to=bar@example.com \
//     -F subject='Hello' \
//     -F text='Testing some Mailgun awesomness!'
//
// response:
//
// {
// "message": "Queued. Thank you.",
// "id": "<20111114174239.25659.5817@samples.mailgun.org>"
// }
//

function send(msg, cb) {
    debug('[id=' + msg.id + '] sending');
    request.post(
        {
            url: 'https://api.mailgun.net/v3/sandboxda341bd3d3d742b291bca56af3f5e24c.mailgun.org/messages',
            auth: {user: 'api', pass: config.mailgun.key},
            form: {from: msg.from,
                   to: msg.to,
                   subject: msg.subject,
                   text: msg.content},
            timeout: 5000
        },
        function(err, resp, body) {
            err = decode_errors(msg, err, resp, body);
            err = err ? 'Mailgun: ' + err : null;
            debug('[id=' + msg.id + '] status: ' + (err || 'ok'));
            cb(err);
        }
    );
}

// This error checking is quick and dirty, and covers the basic cases.
function decode_errors(msg, err, resp, body) {
    if (err) {
        return err;
    }

    // mailgun error codes:
    // https://documentation.mailgun.com/api-intro.html#errors
    switch (resp.statusCode) {
    case 200: return null;
    case 400: return 'Bad Request: Often missing a required parameter';
    case 401: return 'Unauthorized: No valid API key provided';
    case 402: return 'Request Failed: Parameters were valid but request failed';
    case 404: return 'Not Found: The requested item doesn’t exist';
    case 500: case 502: case 503: case 504:
        return 'Server Errors: something is wrong on Mailgun\'s end';
    }
    return 'Error: unknown';
}

// ----------------------------------------------------------------------------

module.exports.send = send;
