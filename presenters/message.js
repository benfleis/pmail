// Message presenter gathers and assembles, and clears messages from
// UI, and sends valid messages on to the message_plex

function MessagePresenter() {
    EventEmitter.call(this);
    this.div = $('.message');
    this.div.find('[name="send"]').click(this.send.bind(this));
}
_.extend(MessagePresenter.prototype, EventEmitter.prototype);

MessagePresenter.prototype.gather = function() {
    return message_from_obj({
        from: this.div.find('[name="from"]').val(),
        to: this.div.find('[name="to"]').val(),
        subject: this.div.find('[name="subject"]').val(),
        content: this.div.find('[name="content"]').val(),
    });
};

MessagePresenter.prototype.clear = function() {
    this.div.find('[name="from"]').val('');
    this.div.find('[name="to"]').val('');
    this.div.find('[name="subject"]').val('');
    this.div.find('[name="content"]').val('');
};

MessagePresenter.prototype.set_result = function(result, is_err) {
    var err_field = this.div.find('[name="error"]');
    err_field.html(result);
    if (is_err) {
        err_field.addClass('error');
    }
    else {
        err_field.removeClass('error');
    }
};

// trigger func from UI
MessagePresenter.prototype.send = function() {
    var msg = this.gather();
    console.log('message', msg);
    var msg_errs = message_get_errors(msg);
    if (msg_errs) {
        this.set_result(msg_errs, true);
    }
    else {
        this.emit('message', msg);
        this.set_result('Sent: id=' + msg.id);
        this.clear();
    }
};

