## PeskyMail
- javascript, via node.js and browser
- simple failure detection with failover to alternative service
- testing: driven by hand tests instead of unit/integration, more notes below


## Install / Run
- mkdir config
- copy valid secrets.json into config/secrets.json
- update Makefile `s/node/nodejs/` if necessary
- `make setup run` or `make setup dev`
- browser to http://localhost:8080


## Tools/Familiarity/Status:
- haven't used node.js actively in >1 year (0.8.x?), so my idioms may be off.
- never been a UI dev, so all bets are off on that side :)
- MVP - never worked in MVP, and only occasionally in MVC; what it
  probably means is that my idea of tight for MVP may vary
  considerably from real world, since I am primarily guided by
  online guides.


## Implementation
- KISS
- MVP: the abstractions are honored, while some details are simplified
  due to the scope of this project; for example, both views are in the
  same index.html, and the model instances are straight JS objects.
- mail tries services in random order
- error handling/decoding at service level is "ok"; at socket level,
  just what comes with socket.io; error checking of the HTTP responses
  would ideally be more thoroughly tested/mocked, and would influence
  retry behaviors and have more unified output (for the client side).
- limited to single to addr, no cc, bcc, etc.
- comments are brief, as most of the functions are relatively trivial,
  and should have names that directly reflect functionality.
- browserify, js unifying tools would be appropriate for a larger
  version, but kept it simple here; models are hand made to work in
  both (chrome, safari)


## MVP
### Models:
- Message: id/ts/from/to/subject/content
- Status: id/ts/status/error

### Views:
- Message: from/to/subject/content fields, send button
- Status: rolling log of status events

### Presenters:
- Message: event proxy for both sides
- Status: ditto


## Code Layout
- server side: `server.js`, `lib/*` and `models/*` via node.js
    - `message_plex.js` handles the senders (`sender_*.js`), failover and status
    - all models are simple JS objects, and used in both node.js and browser
- client side: `views/index.html`, `models/*.js`, `presenters/*.js` and `public/js/*` in browser
    - `index.html` specifies the views, and pulls in the rest of the code
    - `public/js/client.js` binds the websocket and presenters 
    - presenters manage... presentation!


## Testing

Since interaction with actual service is most critical, testing was
all hand based, creating/forcing as many real errors as possible. For
a real product, the follow up would include automated tests which
verify expected behaviors for the run of expected (and unexpected)
failures. UI tested in Chrome and Safari.


## Communications

Chose websocket to give a simple two-way event bus, so my focus
remains on the events, not how they move. For this project, I assume
the websocket bus to be "sufficiently" reliable that I do not worry
(aside from general endpoint timers) about message loss.
